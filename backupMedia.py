# USAGE
# python backupMedia.py -h

# import the necessary packages

import backup_shared
import shutil
import argparse
import os
import time
import re
import logging
import threading
import queue
import io
import traceback
import tempfile
import sys

#import face_recognition.api as face_recognition

str_watch_dir        = "watch_dir"
str_dest_dir         = "dest_dir"
str_mode             = "mode"
str_debug            = "debug"
str_threads          = "threads"
str_recursive        = "recursive"
str_days_older_than  = "days_older_than"
str_file_date_format = "file_date_format"

config_fname         = 'config.json'
env                  = "PRODUCTION"
config               = backup_shared.Config(config_fname, env , "backupMedia")
shared_config        = backup_shared.Config(config_fname, env , "shared")

default_wait_time                       = int(config.get_value("default_wait_time"))  # seconds
log_dir_name                            = config.get_value("log_dir_name")
done_file                               = config.get_value("done_filename")
max_workers                             = int(config.get_value("max_workers"))
worker_get_timeout                      = int(config.get_value("worker_get_timeout"))
blur_image_threshold                    = int(config.get_value("blur_image_threshold"))
exiftool_bin_relative_path              = config.get_value("exiftool_bin_relative_path")
rotate_image_bin_relative_path          = config.get_value("rotate_image_bin_relative_path")
args_file_path                          = config.get_value("args_file_relative_path")
known_faces_dir                         = config.get_value("known_faces_dir")
ext_supported_for_image_mail            = shared_config.get_value("ext_supported_for_image_mail")
duplicates_detector_relative_path       = config.get_value("duplicates_detector_relative_path")
image_blur_detect_bin_relative_path     = config.get_value("image_blur_detect_bin_relative_path")


logger = None
processed_file_path = shared_config.get_value("processed_file_relative_path")

debug_enabled = 0

changed_dirs_hash = {}
# filename date format params (assuming that year is 4 digits, month and day are 2):
start_index_year = -1
start_index_month = -1
start_index_day = -1

# process only files older than parameter
older_than_value = None

class FaceDetect(object):

    known_face_encodings = None
    known_names          = None

    def __init__(self, img_path = None):
        self.img_path = img_path
        self.img_ppl  = []

        if not FaceDetect.known_face_encodings:
            FaceDetect.known_names, FaceDetect.known_face_encodings = self.scan_known_people(config.get_value("known_faces_dir"))

    def get_people_in_img(self):
        return self.img_ppl

    def test_image(self, tolerance=0.6):

        if self.img_path == None:
            return

        unknown_image = face_recognition.load_image_file(self.img_path)
        unknown_encodings = face_recognition.face_encodings(unknown_image)

        for unknown_encoding in unknown_encodings:
            distances = face_recognition.face_distance(FaceDetect.known_face_encodings, unknown_encoding)
            result = list(distances <= tolerance)

            if True in result:
                self.img_ppl += [name for is_match, name, distance in
                 zip(result, FaceDetect.known_names, distances) if is_match]
            else:
                self.img_ppl.append("unknown person")

        if not unknown_encodings:
            # no faces were found in image
            self.img_ppl.append("no person found")


    @staticmethod
    def image_files_in_folder(folder):
        return [os.path.join(folder, f) for f in os.listdir(folder) if re.match(r'.*\.(jpg|jpeg|png)', f, flags=re.I)]

    @staticmethod
    def scan_known_people(known_people_folder):
        known_names = []
        known_face_encodings = []

        for file in FaceDetect.image_files_in_folder(known_people_folder):
            basename = os.path.splitext(os.path.basename(file))[0]
            img = face_recognition.load_image_file(file)
            encodings = face_recognition.face_encodings(img)

            if len(encodings) > 1:
                backup_shared.log_warning(logger, "WARNING: More than one face found in {}. Only considering the first face.".format(file))

            if len(encodings) == 0:
                backup_shared.log_warning(logger, "WARNING: No faces found in {}. Ignoring file.".format(file))
            else:
                known_names.append(basename)
                known_face_encodings.append(encodings[0])

        return known_names, known_face_encodings


def get_args_dest_dir(args):
    if backup_shared.is_windows():
        return args[str_dest_dir]

    if backup_shared.is_linux():
        return backup_shared.linux_default_dest_dir()

    return []

def get_args_watch_dir(args):
    if backup_shared.is_windows():
        return args[str_watch_dir]

    if backup_shared.is_linux():
        return [backup_shared.linux_default_watch_dir()]

    return []

def get_args_mode(args):
    return args[str_mode]


def get_args_threads(args):
    return args[str_threads]


def get_args_recursive(args):
    return args[str_recursive]


def get_args_file_date_format(args):
    return args[str_file_date_format]


def get_args_days_older_than(args):
    return args[str_days_older_than]


def get_args_debugs_should_be_enabled(args):
    return (True if args[str_debug] else False)


def is_debug_enabled():
    return (True if debug_enabled == 1 else False)


def move_file_if_not_exist(file_path, dest_dir):
    try:
        backup_shared.move_file(file_path, dest_dir)
    except shutil.Error as moveError:
        # check if file is already exist and in the same size
        if backup_shared.file_exists(os.path.join(dest_dir, file_path)) and \
                backup_shared.get_size_of_file(os.path.join(dest_dir, file_path)) == \
                backup_shared.get_size_of_file(file_path):
            backup_shared.log_info(logger,
                "destination: " + dest_dir + " and source file: " + file_path + " are in the same size, removing the source file")
            backup_shared.remove_file(file_path)
        else:
            print("moveError = " + str(moveError))
            msg = "error while moving file " + file_path + " to " + dest_dir
            backup_shared.log_error(logger, msg)
            backup_shared.log_error(logger, moveError)


def start_logging(log_file_path):
    global logger

    level = logging.INFO

    if is_debug_enabled():
        level = logging.DEBUG

    logger = backup_shared.init_logging('backup_media', level, log_file_path)


def log_move_file(file_path, dest_dir, desc):
    msg = file_path + " MOVED to " + desc + " folder: " + dest_dir
    backup_shared.log_info(logger, msg)


def move_file_and_log(file_path, dest_dir, log_file, desc):
    move_file_if_not_exist(file_path, dest_dir)
    log_move_file(file_path, dest_dir, desc)


# end of utils functions

def get_first_line(string):
    ret = ""
    if (type(string) != str):
        string = str(string, encoding='utf-8')
    for char in string:
        if char != "\n" and char != "\r" and char != "\t":
            ret += char
        else:
            break

    return ret


def create_temp_args_file(args_file_path, file_path):
    try:
        with io.open(args_file_path, 'w', encoding='utf-8') as f:
            f.write(file_path.encode())
        f.close()
    except IOError:
        backup_shared.log_error(logger, "Error creating temp args file: " + args_file_path)
        print("Error creating temp args file: " + args_file_path)


def get_temp_args_dir():
    if backup_shared.is_windows():
        return os.path.join(os.getcwd(), 'tmp_args')

    if backup_shared.is_linux():
        return os.path.join('/tmp/tmp_args')

    return None


def launch_exiftool_cmd(file_path, prog_list):
    args_dir = get_temp_args_dir()
    
    if not os.path.exists(args_dir):
        os.mkdir(args_dir)

    # create temp file for exiftool

    tmp      = tempfile.NamedTemporaryFile(mode='w+b', delete=False, prefix='args' + str(threading.get_ident()), dir=args_dir)
    tmp_path = os.path.join(args_dir, tmp.name)
    ret      = ""

    try:
        tmp.write(file_path.encode())
        tmp.close()
        ret = backup_shared.launch_cmd(prog_list + ["-charset", "filename=utf8", "-@", tmp_path])
    except Exception as e:
        backup_shared.log_error(logger, "exittool exception: " + str(e))
    finally:
        backup_shared.remove_file(tmp_path)

    return ret


def get_taken_date_of_file(file_path):
    ret = launch_exiftool_cmd(file_path,
                              [exiftool_bin_relative_path, "-createdate", "-d", "%Y-%m-%d", "-p", "$createDate"])
    return get_first_line(ret)


def get_modify_date_of_file(file_path):
    ret = launch_exiftool_cmd(file_path, [exiftool_bin_relative_path, "-FileModifyDate", "-d", "%Y-%m-%d"])
    string = get_first_line(ret)
    # output example: File Modification Date/Time     : 2016-02-27
    str_list = string.split(": ", 1)

    if len(str_list) > 1:
        return str_list[1]
    return ""


def validate_args(args):
    if args["mode"] != "0" and args["mode"] != "1":
        backup_shared.log_error(logger, "not a valid mode!")
        print("not a valid mode!")
        return -1

    for wdir in get_args_watch_dir(args):
        if not backup_shared.dir_exists(wdir):
            backup_shared.log_error(logger, "watch dir doesn't exist! info: " + wdir)
            print("watch dir doesn't exist! info:", str(wdir.encode("utf-8")))
            return -1

    if not backup_shared.dir_exists(get_args_dest_dir(args)):
        backup_shared.log_error(logger, "destination dir doesn't exist!")
        print("destination dir doesn't exist!")
        return -1

    return 0


def rotate_file(file_path):
    backup_shared.launch_cmd([rotate_image_bin_relative_path, file_path])


def launch_duplicates_detector(directory_path, delete_duplicates=False):
    return backup_shared.launch_cmd(
        [sys.executable, duplicates_detector_relative_path, ("1" if delete_duplicates else "0"), directory_path], False)


def file_blurry(file_path):
    # current implementation checks only images
    ret = backup_shared.launch_cmd(
        [sys.executable, image_blur_detect_bin_relative_path, "-t", str(blur_image_threshold), "-n", "-f", file_path], False)
    ret = get_first_line(ret)
    backup_shared.log_debug(logger, "detect_blur returned '" + str(ret) + "'")
    if is_debug_enabled():
        print("detect_blur returned '" + str(ret) + "'")

    ret = get_first_line(ret)
    return (True if ret == "1" else False)


def q_worker_thread(q):
    thread_name = threading.current_thread().name
    while True:
        try:
            item = q.get(True, worker_get_timeout)
        except queue.Empty:
            backup_shared.log_debug(logger, "q_worker_thread: " + thread_name + " got an empty queue for " + str(
                worker_get_timeout) + " seconds, thread is terminating. q len = " + str(q.qsize()))
            if is_debug_enabled():
                print("q_worker_thread: ", thread_name, "got an empty queue for", worker_get_timeout,
                      "seconds, thread is terminating. q len = ", q.qsize())
            break

        item_str = ""
        for k in item:
            item_str += "[" + str(k) + "]="
            item_str += "<" + str(item[k]) + "> "

        backup_shared.log_debug(logger, "q_worker_thread: " + thread_name + " item is: " + item_str)
        if is_debug_enabled():
            print("q_worker_thread: ", thread_name, "item is: ", item_str)
        try:
            item['func'](*item['args'])
        except:
            backup_shared.log_error(logger, "exception!!! " + item_str)
            print("exception!!!!", item_str)

        q.task_done()
        backup_shared.log_debug(logger, "q_worker_thread: " + thread_name + " finished working on item: " + item_str)
        if is_debug_enabled():
            print("q_worker_thread: ", thread_name, "finished working on item: ", item_str)


def start_q_workers(q):
    threads = []
    for i in range(max_workers):
        t = threading.Thread(target=q_worker_thread, args=(q,))
        threads.append(t)
        t.start()

    return threads


def init_file_data_format_params(arg):
    global start_index_year
    global start_index_month
    global start_index_day

    start_index_year = arg.index('Y')
    start_index_month = arg.index('M')
    start_index_day = arg.index('D')


def check_file_date_format_arg(arg):
    # TODO: validate 4 Y, 2 M and 2D in the right pattern
    Y_found = False
    M_found = False
    D_found = False

    for ch in arg:
        if ch != "X" and ch != "Y" and ch != "M" and ch != "D":
            return False
        else:
            if ch == "Y":
                Y_found = True
            if ch == "M":
                M_found = True
            if ch == "D":
                D_found = True

    return Y_found and M_found and D_found


def handle_duplicate_files_in_dir(dir):
    try:
        backup_shared.log_debug(logger, "inside thread lock for duplicates detector")
        if is_debug_enabled():
            print("inside thread lock for duplicates detector")
        # find duplicates on destination dir
        ret = launch_duplicates_detector(dir, True)
        ret = str(ret, encoding="utf-8")
        backup_shared.log_info(logger, "Duplicates detector output: ")
        backup_shared.log_info(logger, ret)
    finally:
        if is_debug_enabled():
            print("getting out of thread lock for duplicates detector")
        backup_shared.log_debug(logger, "getting out of thread lock for duplicates detector")


def add_changed_file(dir, filename):
    lock = threading.Lock()
    lock.acquire()
    if not backup_shared.file_exists(os.path.join(dir, filename)):
        # can happen if file was removed due to duplication
        lock.release()
        return
    try:
        if changed_dirs_hash.get(dir):
            changed_dirs_hash[dir].append(filename)

        else:
            changed_dirs_hash[dir] = []
            changed_dirs_hash[dir].append(filename)

    finally:
        lock.release()


def exif_add_tag(file_path, person):

    ret = launch_exiftool_cmd(file_path,
                        [exiftool_bin_relative_path, "-Subject+=" + person, "-overwrite_original", "-preserve"])

    res = get_first_line(ret)
    backup_shared.log_debug_and_print(logger, "adding tag '" + person + "' via exif, result: " + str(res))


def add_faces_to_img(file_path, ppl):
    for person in ppl:
        exif_add_tag(file_path, person)


def do_face_recog(file_path):
    
    if backup_shared.get_lower_ext_from_file(file_path) not in ext_supported_for_image_mail:
        return

    # check image for faces
    faces = FaceDetect(file_path)
    faces.test_image()
    ppl = faces.get_people_in_img()

    # add faces to image tags meta-data
    add_faces_to_img(file_path, ppl)


def do_action(file_path):
    # no face recognition till further notice, not that accurate
    # do_face_recog(file_path)
    pass


def do_action_on_file(file_path, dest_dir, log_file_path, re_date):
    dir_candidate_str = ""
    desc = ""
    file_date = ""
    backup_shared.log_debug(logger, "will do format style change")
    filename = backup_shared.get_filename_from_path(file_path)
    backup_shared.log_debug(logger, filename)

    # format file
    if start_index_year != -1:
        dir_candidate_str = filename[start_index_year:start_index_year + 4] + "-" + filename[
                                                                                    start_index_month:start_index_month + 2] + "-" + filename[
                                                                                                                                     start_index_day:start_index_day + 2] + "_Unfiltered"
        backup_shared.log_debug(logger, "dir candidate: " + dir_candidate_str)
        desc = "format file"

    if dir_candidate_str == "":
        taken_date = (get_taken_date_of_file(file_path))
        backup_shared.log_debug(logger, "taken_date: '" + taken_date + "'")
        if is_debug_enabled():
            print("taken_date: '" + taken_date + "'")

        if re_date.match(taken_date):
            desc = "taken date"
            file_date = taken_date
            dir_candidate_str = taken_date + "_Unfiltered"

    # if create date wasn't found
    if dir_candidate_str == "":
        modify_date = get_modify_date_of_file(file_path)
        backup_shared.log_debug(logger, "modify_date: " + modify_date)
        if is_debug_enabled():
            print("modify_date: ", modify_date)

        if re_date.match(modify_date):
            desc = "modify date"
            file_date = modify_date
            dir_candidate_str = modify_date + "_Unfiltered"

    if older_than_value and file_date != "":
        # check if file should be processed
        days_diff = backup_shared.days_between(file_date, backup_shared.get_current_date())
        if int(older_than_value) > days_diff:
            # don't
            print("do_action_on_file: file " + file_path + " is " + str(
                days_diff) + " days old, therefore won't be processed")
            backup_shared.log_debug(logger, "do_action_on_file: file " + file_path + " is " + str(
                days_diff) + " days old, therefore won't be processed")
            return

    # if modify date wasn't found also
    if dir_candidate_str == "":
        desc = "generic"
        dir_candidate_str = "Generic_Unfiltered"

    if file_blurry(file_path) == True:
        # move to a proper dir
        desc = "blurry"
        dir_candidate_str = os.path.join(dir_candidate_str, "Deletion_Candidates")

    # due to bug found in 3/10/17 - file is lost during rotation, followin
    # rotate_file(file_path)

    do_action(file_path)
    dir_candidate = os.path.join(dest_dir, dir_candidate_str, "")
    backup_shared.create_dir_if_needed(dir_candidate, logger)

    backup_shared.log_debug(logger, "dir candidate is: " + dir_candidate)
    if is_debug_enabled():
        print("dir candidate is: ", dir_candidate)

    move_file_and_log(file_path, dir_candidate, log_file_path, desc)

    add_changed_file(dir_candidate, filename)


def do_action_on_watch_folder(watch_dir, dest_dir, log_file_path, threads_enabled, q, recursive):
    if is_debug_enabled():
        print(do_action_on_watch_folder.__name__, str(watch_dir.encode("utf-8")), str(dest_dir.encode("utf-8")), str(log_file_path.encode("utf-8")))

    for file in os.listdir(watch_dir):

        if backup_shared.is_dir(os.path.join(watch_dir, file)):
            if recursive == True:
                backup_shared.log_info(logger, file + " is a directory, going in...")
                print(str(file.encode("utf-8")) + " is a directory, going in...")
                do_action_on_watch_folder(os.path.join(watch_dir, file), dest_dir, log_file_path, threads_enabled, q,
                                          recursive)
            else:
                backup_shared.log_info(logger, file + " is a directory, and we're not in recursive mode. pass...")
                print(str(file.encode("utf-8")) + " is a directory, and we're not in recursive mode. pass...")

        else:
            full_file_path = os.path.join(watch_dir, file)
            print("working on file: ", str(full_file_path.encode("utf-8")))
            backup_shared.log_info(logger, "working on file: " + full_file_path)
            re_date = re.compile(r"^\d{4}[-]\d[\d]?[-]\d[\d]?$")  # Pattern for date
            if threads_enabled:
                q_item = {}
                q_item['func'] = do_action_on_file
                q_item['args'] = (full_file_path, dest_dir, log_file_path, re_date)
                q.put(q_item)
            else:
                try:
                    do_action_on_file(full_file_path, dest_dir, log_file_path, re_date)
                except Exception as e:
                    backup_shared.log_info(logger, "action on file '{:}' failed: {:}".format(full_file_path, str(e)))


def handle_processed_dirs_duplicates(dirs_dict):
    for dir in dirs_dict:
        print("checking for duplicates in dir " + str(dir.encode("utf-8")))
        backup_shared.log_info(logger, "checking for duplicates in dir " + dir)
        handle_duplicate_files_in_dir(dir)


def export_processed_files_into_file(_processed_file_path):
    with io.open(_processed_file_path, 'w', encoding='utf-8') as f:
        for dir in changed_dirs_hash:
            for file in changed_dirs_hash[dir]:
                file_path = str(os.path.join(dir, file))

                # file may have been removed during duplicates check
                if backup_shared.file_exists(file_path):
                    backup_shared.log_debug(logger, "exporting file " + file_path)
                    f.write('{0}\r\n'.format(os.path.join(dir, file)))
    f.close()


def main(args, ap):
    # turn on debugs (if needed)
    if get_args_debugs_should_be_enabled(args):
        global debug_enabled
        debug_enabled = 1
        print("ENABLING DEBUGS")

    log_dir = os.path.join(backup_shared.get_current_wd(), log_dir_name)
    log_file_path = backup_shared.build_log_file_path(log_dir, done_file)
    start_logging(log_file_path)

    # Start of main logic
    if validate_args(args) != 0:
        return

    backup_shared.log_info(logger, "Start time: " + backup_shared.get_current_time())

    # create destination generic dir
    backup_shared.create_dir_if_needed(os.path.join(get_args_dest_dir(args), "Generic_Unfiltered"), logger)

    # init format params
    file_date_format_arg = get_args_file_date_format(args)

    # init face detect
    #FaceDetect()

    if file_date_format_arg:
        if check_file_date_format_arg(file_date_format_arg) == False:
            ap.error("the argument should be filename pattern.\nFor proper use, please see help")
        init_file_data_format_params(file_date_format_arg)

    global older_than_value
    older_than_value = get_args_days_older_than(args)

    threads_enabled = (True if get_args_threads(args) else False)

    if threads_enabled == True:
        backup_shared.log_debug_and_print(logger, "Starting workers")
        q = queue.Queue()
        start_q_workers(q)
        backup_shared.log_debug_and_print(logger, "Workers started")

    else:
        q = None

    recursive = (True if get_args_recursive(args) else False)

    while True:
        for dir in get_args_watch_dir(args):
            try:
                do_action_on_watch_folder(dir, get_args_dest_dir(args), log_file_path, threads_enabled, q, recursive)
            except (NameError, IOError, PermissionError, FileNotFoundError):
                trace = traceback.format_exc()
                backup_shared.log_debug(logger, trace)
                print("Exception occurred: " + trace)

        if get_args_mode(args) == "0":
            break

        time.sleep(default_wait_time)

    if threads_enabled == True:
        # wait for all the tasks to finish
        backup_shared.log_debug_and_print(logger, "joining all the tasks")
        q.join()

        backup_shared.log_debug(logger, "all tasks have finished")
        print("all tasks have finished")

    handle_processed_dirs_duplicates(changed_dirs_hash)

    backup_shared.log_info(logger, "Tasks End time: " + backup_shared.get_current_time())

    export_processed_files_into_file(processed_file_path)


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--mode", default="0",
                help="mode: 0 - for one time running (default)\n      1 - for continuously watching directories")
ap.add_argument("-dd", "--dest_dir", required=True,
                help="destination folder for media files")
ap.add_argument("-wd", "--watch_dir", required=True, nargs='+',
                help="folder to be watched for media files to be processed")
ap.add_argument("-d", "--debug", action='store_true',
                help="turn on debugs")
ap.add_argument("-fdf", "--file_date_format",
                help="copy files into directories besed on their filename structure (should contain date) - the argument should be filename pattern.\nexample for argument: XXYYYYMMDD - will copy file IM20201231 into folder with the date of 2020-12-31 (X is wildcard). after year + month + day has been given, Xs shouldn't be written")
ap.add_argument("-do", "--days_older_than",
                help="process only files older than X days")
ap.add_argument("-t", "--threads", action='store_true',
                help="turn on threads (with number of threads as parameter)")
ap.add_argument("-r", "--recursive", action='store_true',
                help="Do recursive action on folders (folder inside a folder")

args = vars(ap.parse_args())

main(args, ap)
