# USAGE
# python detect_blur.py [--images images] [--full_path image_path] [--num] [--threshold]

# import the necessary packages
from imutils import paths
import argparse
import cv2

def variance_of_laplacian(image):
	# compute the Laplacian of the image and then return the focus
	# measure, which is simply the variance of the Laplacian
	return cv2.Laplacian(image, cv2.CV_64F).var()

def process_image(imagePath, res_only):
	# load the image, convert it to grayscale, and compute the
	# focus measure of the image using the Variance of Laplacian
	# method
	if res_only == 0:
		print("Now checking: " + imagePath)
	
	blurry = 0
	text = ""
	
	try:
		image = cv2.imread(imagePath)
		#the following line was commented out due to assertion on every photo
		#assert image

		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		fm = variance_of_laplacian(gray)
			# if the focus measure is less than the supplied threshold,
			# then the image should be considered "blurry"
		if fm < args["threshold"]:
			blurry = 1
			text = "Blurry"
		else:
			blurry = 0
			text = "Not Blurry"

		text += ": " + str(round(fm, 2))

	except:
		if res_only == 0:
			print("Error on handling: " + imagePath)
	

	if res_only == 0:
		print(imagePath + " " + text)
	else:
		print(blurry)
		
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images",
	help="path to input directory of images")
ap.add_argument("-t", "--threshold", type=float, default=100.0,
	help="focus measures that fall below this value will be considered 'blurry'")
ap.add_argument("-f", "--full_path", 
	help="full path to input image")	
ap.add_argument("-n", "--num", action='store_true',
	help="numeric result output - 1 for blur, 0 for not-blur; usage -n/--num 1")	
args = vars(ap.parse_args())

if args["num"]:
	res_only = 1
else:
	res_only = 0

if args["images"]:
	# loop over the input images
	for imagePath in paths.list_images(args["images"]):
		process_image(imagePath, res_only)

if args["full_path"]:
	process_image(args["full_path"], res_only)