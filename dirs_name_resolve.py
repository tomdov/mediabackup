import os
import argparse
import shutil
import io
import codecs

log_file = os.path.join("dirs_name_resolve_log.txt")

def remove_file(file_path):
	os.remove(file_path)
	
def parse_directory(watch_dir, dest_dir):

	logf = codecs.open(log_file, 'w', encoding='utf-8')
		
	for file in os.listdir(watch_dir):
		#shouldn't be anything different from txt file
		if (file[-4:] != ".txt"):
			log_str = 'file ' + str(file) + ' is not txt file, skipping...' + '\r\n'
			logf.write(log_str)
			continue

		logf.write('Working on file ' + str(file) + '\r\n')
		
		#open the file and get first line of it
		full_file_path = os.path.join(watch_dir, file)

		with io.open(full_file_path,'r',encoding='utf-8') as f:
			first_line = f.readline()
			
			if (first_line == ""):
				log_str = 'file ' + str(file) + ' is empty, skipping...' + '\r\n'
				logf.write(log_str)
				f.close()
				continue
			
			first_line = first_line.replace("\n", "").replace("\r", "")
		
		f.close()

		# not an empty file
		#remove the file extension
		file_without_ext = file[:file.rfind('.')]
		
		#append the first_line to the dir name
		try:
			orig_dir = os.path.join(dest_dir, file_without_ext)
			dest_dir_temp = os.path.join(dest_dir, file_without_ext + "_" + first_line)
			shutil.move(orig_dir, dest_dir_temp)
			log_str = 'Directory ' + str(orig_dir) + ' was renamed to ' + str(dest_dir_temp) + '\r\n'
			logf.write(log_str)
			#remove the resolve file
			remove_file(full_file_path)
		except:
			logf.write("Error during move of directory - file " + file.decode('utf-8') + "\n")
			
	logf.close()
	
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--directory", 
	help="Directory path for files to be parsed")	
ap.add_argument("-dp", "--destination_path", 
	help="Destination directory path of directories that should be renamed")	
args = vars(ap.parse_args())

if args["directory"] and args["destination_path"]:
	
	parse_directory(args["directory"], args["destination_path"])
else:
	print("Both directory and destination directory should be provided!")
