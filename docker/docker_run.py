#!/usr/bin/python3

import argparse
import os
import subprocess
import sys

sys.path.insert(0, '..')
import backup_shared

def check_directory_exists(directory_path):
    return os.path.isdir(directory_path)

def default_sync_src_drv():
    return "/tmp/drv/src/"

def default_sync_dst_drv():
    return "/tmp/drv/dst/"

def docker_run(args):
    watch_dir = args.watch_dir
    dest_dir  = args.dest_dir
    sync_src  = args.sync_src_dir
    sync_dst  = args.sync_dst_dir

    if not check_directory_exists(watch_dir):
        print(f"Error: Watch directory '{watch_dir}' does not exist.")
        return

    if not check_directory_exists(dest_dir):
        print(f"Error: Destination directory '{dest_dir}' does not exist.")
        return

    vols = [
        ('$PWD/../', '/mb/', 'ro'),
        ('$PWD/../MediaLogs', '/mb/MediaLogs', 'rw'),
        (watch_dir, backup_shared.linux_default_watch_dir(), 'rw'),
        (dest_dir, backup_shared.linux_default_dest_dir(), 'rw'),
        (sync_src, default_sync_src_drv(), 'ro'),
        (sync_dst, default_sync_dst_drv(), 'rw')
    ]

    vols = [f"-v {src}:{dest}:{perm}" for src, dest, perm in vols]
    vols = " ".join(vols)

    cmd = f'docker run -it {vols} --user {args.user}:{args.group} -e "SD={default_sync_src_drv()}" -e "DD={default_sync_dst_drv()}" mediabackup'
    print(cmd)

    p = subprocess.Popen(cmd, shell=True)
    p.communicate() 

def media_docker_run(subargs):
    parser = argparse.ArgumentParser(description="MediaBackup docker run")
    parser.add_argument("-w", "--watch-dir", required=True, help="The directory to watch for changes.")
    parser.add_argument("-d", "--dest-dir", required=True, help="The destination directory for copied files.")
    parser.add_argument("-sd", "--sync-src-dir", required=True, help="The source directory to synchronize.")
    parser.add_argument("-u", "--user", required=False, default=1000, help="user id which will run inside the container")
    parser.add_argument("-g", "--group", required=False, default=1000, help="user group which will run inside the container")
    parser.add_argument("-dd", "--sync-dst-dir", required=True,
                        help="The destination directory for synchronized files.")

    args = parser.parse_args(subargs)

    docker_run(args)

def media_docker_build(subargs):
    p = subprocess.Popen("docker build . -t mediabackup", shell=True)
    p.communicate()

if __name__ == "__main__":
    sysargs = sys.argv[1:]

    parser  = argparse.ArgumentParser(description="MediaBackup docker launcher")
    parser.add_argument("cmd", choices=["build", "run"], help="launch the media backup docker")

    args = parser.parse_args(sysargs[:1])

    if args.cmd == "build":
        media_docker_build(sysargs[1:])
    elif args.cmd == "run":
        media_docker_run(sysargs[1:])
