# import the necessary packages
import imutils
import argparse
import cv2
import os
import json

config = None
with open('config.json', 'r') as f:
	config = json.load(f)
	
env = "PRODUCTION"
shared_config = config[env]["shared"]

ext_supported_for_image_mail = 	shared_config["ext_supported_for_image_mail"]
ext_supported_for_video_mail = 	shared_config["ext_supported_for_video_mail"]

def get_lower_ext_from_file(path):
	_path, ext = os.path.splitext(path)
	return ext[1:].lower()

def remove_file(file_path):
	os.remove(file_path)
	
def video_to_frames(video_filename):
    #Extract frames from video
	cap = cv2.VideoCapture(video_filename)
	video_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) - 1

	frames = []
	if cap.isOpened() and video_length > 0:
		frame_ids = [0]
		if video_length >= 4:
			frame_ids = [0, 
						round(video_length * 0.25), 
						round(video_length * 0.5),
						round(video_length * 0.75),
						video_length - 1]
		count = 0
		success, image = cap.read()
		while success:
			if count in frame_ids:
				frames.append(image)
			success, image = cap.read()
			count += 1
	return frames
	
def process_video(path, output_path, max_size):
	print("now making video thumbnail " + path)
	
	try:
		frames = video_to_frames(path)
		input_temp_pic_path = os.path.join('vid_temp.png')
		# currently pass only the middle frame
		if len(frames) > 2:
			cv2.imwrite(input_temp_pic_path, frames[2])
			process_image(input_temp_pic_path, output_path, max_size)
			remove_file(input_temp_pic_path)
	except:
		print("Error on handling video: " + path)
		
def process_image(imagePath, output_path, max_size):
	# load the image, and make it a thumbnail
	print("Now checking: " + imagePath)
	
	try:
		image = cv2.imread(imagePath)
		#the following line was commented out due to assertion on every photo
		#assert image
		_width, _height = image.shape[:2] 
		if _width > _height:
			imRes = imutils.resize(image, width = max_size)
		else:
			imRes = imutils.resize(image, height = max_size)
		
		cv2.imwrite(output_path, imRes);
		
	except:
		print("Error on handling: " + imagePath)
		
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--full_path", 
	help="full path to input image")	
ap.add_argument("-dp", "--destination_path", 
	help="full path to output image")	
ap.add_argument("-ms", "--max_pixel_size", 
	help="max size of thumbnail image")	
	
args = vars(ap.parse_args())

if args["full_path"] and args["destination_path"] and args["max_pixel_size"]:
	ext = get_lower_ext_from_file(args["full_path"])

	if ext in ext_supported_for_image_mail:
		process_image(args["full_path"], args["destination_path"], int(args["max_pixel_size"]))
	elif ext in ext_supported_for_video_mail:
		process_video(args["full_path"], args["destination_path"], int(args["max_pixel_size"]))
else:
	print("full path of input file and output image should be provided + max size!")