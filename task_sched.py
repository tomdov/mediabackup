import backup_shared
import time
import os
import logging
import importlib
import sys
import io
import argparse

config_fname = 'config.json'
env          = 'PRODUCTION'
logger       = None

if not os.path.exists(config_fname):
    print(f"config file {config_fname} must exist")
    sys.exit(-1)

config        = backup_shared.Config(config_fname, env, 'task_sched')
shared_config = backup_shared.Config(config_fname, env, 'shared')

file_sync_path                   = config.get_value("file_sync_path")
smart_ctl_path                   = config.get_value("smart_ctl_path")
log_directory                    = config.get_value("log_dir_name")
log_filename                     = config.get_value("log_filename")
smart_ctl_output_path            = os.path.join(config.get_value("smart_ctl_output_dir"), "smartctl_")
send_email_path                  = os.path.join(config.get_value("send_email_relative_dir"), "sendEmail.exe")
backup_script                    = config.get_value("backup_script_relative_path")
resolve_files_enabled            = config.get_value("resolve_files_enabled")
resolve_files_path               = config.get_value("resolve_files_relative_path")
thumb_files_path                 = config.get_value("thumb_files_relative_path")
thumbnail_maker_path             = config.get_value("thumbnail_maker_relative_path")
resolve_files_shared_folder_link = config.get_value("resolve_files_shared_folder_link")
dirs_name_resolve_log_file       = config.get_value("dirs_name_resolve_log_file_relative")
processed_file_path              = shared_config.get_value("processed_file_relative_path")
ext_supported_for_image_mail     = shared_config.get_value("ext_supported_for_image_mail")
ext_supported_for_video_mail     = shared_config.get_value("ext_supported_for_video_mail")

attachment_list = []

max_thumb_size = config.get_value("max_thumb_pixel_size")

mailFrom    = config.get_value("mail")["mailFrom"]
mailTo      = config.get_value("mail")["mailTo"]
mail_config = config.get_value("mail")

class ResolveFiles:
    @staticmethod
    def handle():
        mail_html = ResolveFiles.generate_resolve_files_mail()
        ResolveFiles.send_summary_mail(mail_html)
        ResolveFiles.remove_files_list(attachment_list)

    @staticmethod
    def send_summary_mail(mail_html):
            # ResolveFiles.add_inline_content_id(msgRoot)

            try:
                send_mail(mailFrom, mailTo, 'Backup summary | ' + time.strftime("%d/%m/%Y"), mail_html)
            except Exception as ex:
                backup_shared.log_error_and_print(logger, "send_general_summary_mail failed: " + str(ex))

    @staticmethod
    def generate_resolve_files_mail():
        processed_files_dict = {}

        ResolveFiles.get_files_into_dict(processed_files_dict)

        mail_html = '<p>Hello, this is summary of processed backup of media<p/>'
        mail_html += '<p>For updating folder names, follow <a href=' + resolve_files_shared_folder_link + '>Resolve files link<a/><p/>'
        mail_html += ResolveFiles.create_html_table_for_files(processed_files_dict, 3)

        ResolveFiles.create_resolve_files(processed_files_dict, resolve_files_path)

        return mail_html

    @staticmethod
    def get_files_into_dict(processed_files_dict):
        with io.open(processed_file_path, 'r', encoding='utf-8') as f:
            for line in f:
                dir = backup_shared.get_dirname_from_path(line)
                filename = backup_shared.get_filename_from_path(line).replace("\r", "").replace("\n", "")

                if not processed_files_dict.get(dir):
                    processed_files_dict[dir] = []

                processed_files_dict[dir].append(filename)

    @staticmethod
    def create_resolve_files(files_dict, path):
        for dir in list(files_dict.keys()):
            # create resolution files in directory
            ResolveFiles.create_resolve_file_for_dir(os.path.basename(dir), path)

    @staticmethod
    def create_resolve_file_for_dir(file, path):
        resolve_file = os.path.join(path, file + ".txt")
        if not backup_shared.file_exists(resolve_file):
            f = open(resolve_file, "w+")
            f.close()

    @staticmethod
    def add_dir_to_mail_html(dir, html):
        html += "<tr> \
            <th><p>" + dir + "</p></th>\
            </tr>"

        return html

    @staticmethod
    def add_pic_to_mail_html(pic, html):
        html += "<th><img src=\"cid:" + pic + "\"/></th>"
        return html

    @staticmethod
    def is_file_supported(path):
        ext = backup_shared.get_lower_ext_from_file(path)

        if (ext in ext_supported_for_image_mail) or (ext in ext_supported_for_video_mail):
            return True
        else:
            return False

    @staticmethod
    def add_pic_to_attachments(path, attachments_list):
        if ResolveFiles.is_file_supported(path):
            attachments_list.append(path)
        else:
            backup_shared.log_error_and_print(logger, "file " + path + " is not supported")

    @staticmethod
    def make_thumb(full_input_path, full_output_path):
        backup_shared.launch_cmd([thumbnail_maker_path, "-f", full_input_path, "-dp", full_output_path, "-ms", max_thumb_size], True)

    @staticmethod
    def create_html_table_for_files(files_dict, elements_in_row):
        mail_html = "<table style=\"width:100%\">"

        # index for files
        i = 0
        # work on each directory - chronologic order
        for dir in sorted(files_dict.keys()):

            mail_html = ResolveFiles.add_dir_to_mail_html(os.path.basename(dir), mail_html)
            mail_html += "<tr>"
            pic_idx = 0

            for file in files_dict[dir]:
                # want only elements_in_row files in a row

                if pic_idx > 0 and pic_idx % elements_in_row == 0:
                    mail_html += "</tr><tr>"

                orig_file = os.path.join(dir, file)
                thumb_dest_file = os.path.join(thumb_files_path, file)

                if ResolveFiles.is_file_supported(orig_file):
                    i += 1
                    pic_idx += 1

                    ext = backup_shared.get_lower_ext_from_file(orig_file)
                    if ext in ext_supported_for_video_mail:
                        thumb_dest_file += '.png'

                    backup_shared.log_info_and_print(logger, "Making thumbnail for: " + str(orig_file) + " in destination: " + str(thumb_dest_file))
                    ResolveFiles.make_thumb(orig_file, thumb_dest_file)
                    ResolveFiles.add_pic_to_attachments(thumb_dest_file, attachment_list)
                    mail_html = ResolveFiles.add_pic_to_mail_html("image" + str(i), mail_html)

            mail_html += "</tr>"

        mail_html += "</table>"

        return mail_html

    @staticmethod
    def add_inline_content_id(msgRoot):
        # index for content ID
        i = 0
        # This example assumes the image is in the current directory
        for file in attachment_list:
            i += 1

            try:
                fp = open(file, 'rb')
                msgImage = MIMEImage(fp.read())
                fp.close()

                # Define the image's ID as referenced above
                msgImage.add_header('Content-ID', '<image' + str(i) + '>')
                msgRoot.attach(msgImage)
            except IOError as e:
                backup_shared.log_info_and_print(logger, str(e))
            except FileNotFoundError as e:
                backup_shared.log_info_and_print(logger, str(e))

    @staticmethod
    def remove_files_list(files_list):
        # remove all files in list
        for file in files_list:
            backup_shared.remove_file(file)


def get_first_line(str):
    ret = ""
    for char in str:
        if char != "\n" and char != "\r" and char != "\t":
            ret += char
        else:
            break

    return ret


def send_mail(sender, to, subject, mail_html, attachments = None):
    route_to_mail(mail_config, sender, to, subject, mail_html, attachments)


def route_to_mail(mail_config, sender, to, subject, mail_html, attachments):
    # generic way to call the relevant mailer and send mail
    mailer      = importlib.import_module('mailers.' + mail_config['mail_adapter'])
    mailer_impl = mailer.MailerImpl({'mail_api_key': mail_config['mail_api_key']})

    mailer_impl.send_mail(sender, to, subject, mail_html, attachments)


def send_general_summary_mail(mail_html, attachments_list):
    try:
        send_mail(mailFrom, mailTo, "Backup was running", mail_html, attachments_list)
    except Exception as ex:
        backup_shared.log_error_and_print(logger, "send_general_summary_mail failed: " + str(ex))


def start_logging(log_file):
    global logger

    level = logging.DEBUG

    logger = backup_shared.init_logging('task_sched', level, log_file)

def get_smart_options():
    return [[], ["-d", "sat"], ["-d", "sat"]]

def run_smart_tests(drives):
    if env != "PRODUCTION":
        return

    if backup_shared.is_linux():
        # no running on linux docker
        return

    drives_smart_options = get_smart_options()
    drives_start_smart_test_options = [["-t", "short", "-C"], ["-t", "conveyance", "-C"]]


    for test in drives_start_smart_test_options:
        for drv, opt in zip(drives, drives_smart_options):
            ret = backup_shared.launch_cmd([smart_ctl_path, drv] + opt + test)
            ret = str(ret, encoding='utf-8')
            backup_shared.log_debug(logger, "Returned for drive " + drv + " and test " + str(test) + " = " + ret)

def run_ext_smart_tests(drives, f):
    if backup_shared.is_linux():
        # no running on linux docker
        return

    smartctl_output_files_list = []
    # Extended drives tests
    for drv, opt in zip(drives, get_smart_options()):
        ret = backup_shared.launch_cmd([smart_ctl_path, drv] + opt + ["-a"])
        ret = str(ret, 'utf-8')

        backup_shared.log_debug(logger, "Returned for drive " + drv + " = " + ret)

        smart_ctl_output_file = smart_ctl_output_path + drv[0:1] + ".txt"
        smartctl_output_files_list += [smart_ctl_output_file]
        smartf = open(smart_ctl_output_file, 'w')
        smartf.write(ret)
        smartf.close()

        summary_index = ret.find("SMART overall-health self-assessment test result")
        f += '<p>Drive ' + drv + ' Smart test result:</p>'
        f += '<p>' + get_first_line(ret[summary_index:]) + '</p>'
        f += '<br/>'

        backup_shared.log_debug(logger, "Summary: " + f)

    return smartctl_output_files_list

def get_drives(args):
    return [args.src_drive_dir, args.dst_drive_dir]

def sync_drives(args):
    if backup_shared.is_windows():
        backup_shared.launch_cmd([file_sync_path, config.get_value("file_sync_batch_file_relative_path")])
    elif backup_shared.is_linux():
        backup_shared.launch_cmd(['rsync', '-av', '--delete', '--progress', args.src_drive_dir, args.dst_drive_dir])

def main_do(args):
    backup_shared.log_info(logger, "Start time: " + backup_shared.get_current_time())

    # start SMART test of external HDD
    drives = get_drives(args)

    if len(drives) == 0 or len(drives) == 1 and drives[0] == '':
        backup_shared.log_error_and_print(logger, "drives list can not be empty!")
        sys.exit(-1)

    run_smart_tests(drives)

    # sleep in order to let Dropbox sync
    time.sleep(int(config.get_value("time_delay_for_sync")))

    free_space_before = []
    free_space_after = []

    for drv in drives:
        free_space_before.append(backup_shared.get_free_space_kb(drv))

    # Run the main backup script
    backup_shared.log_debug(logger, "Before backup script: " + backup_shared.get_current_time())
    ret = backup_shared.launch_cmd(
        [sys.executable, backup_script, "-m", "0", "-d", "-wd"] + config.get_value("watch_dirs") + ["-do"] + [config.get_value("work_on_files_older_than")] + [
            "-dd"] + [config.get_value("files_dest_dir")])
    ret = str(ret, encoding='utf-8')

    backup_shared.log_debug(logger, "Dump of backup script output: " + ret)

    # process the files that were created during media backup
    backup_shared.log_debug(logger, "After backup script: " + backup_shared.get_current_time())

    if resolve_files_enabled:
        ResolveFiles.handle()
    else:
        backup_shared.log_debug(logger, "NOT sending summary mail as feature is being disabled")

    backup_shared.log_debug(logger, "After sending summary mail: " + backup_shared.get_current_time())

    # sync the files between the HDD cluster
    backup_shared.log_debug(logger, "Before syncing files: " + backup_shared.get_current_time())
    sync_drives(args)
    backup_shared.log_debug(logger, "After syncing files: " + backup_shared.get_current_time())

    # Checking free space after operations
    for drv in drives:
        free_space_after.append(backup_shared.get_free_space_kb(drv))

    # generic mail file
    f = ""

    for drv, ds_before, ds_after in zip(drives, free_space_before, free_space_after):
        f += '<p>Drive {0}</p>'.format(drv)
        f += '<p>Disk space before operation: {0:,} KB</p>'.format(ds_before)
        f += '<p>Disk space after operation: {0:,} KB</p>'.format(ds_after)
        f += '<p>Delta Free Disk space: {0:,} KB</p>'.format(int(ds_after - ds_before))
        f += '<br/>'

    backup_shared.log_info(logger, f)

    smartctl_output_files_list = run_ext_smart_tests(drives, f)

    # send mail in the end
    backup_shared.log_debug(logger, "Sending the generic mail")
    send_general_summary_mail(f, smartctl_output_files_list)

    backup_shared.log_info(logger, "End time: " + backup_shared.get_current_time())

def args_parse():
    ap = argparse.ArgumentParser()
    ap.add_argument("-dd", "--dst_drive_dir", required=True,
                    help="destination folder to be synced")
    ap.add_argument("-sd", "--src_drive_dir", required=True,
                    help="source folder to be synced")

    print(sys.argv)
    return ap.parse_args(sys.argv[1:])

try:
    # Starting with init logging                                         #
    log_dir = os.path.join(backup_shared.get_current_wd(), log_directory)
    log_file_path = backup_shared.build_log_file_path(log_dir, log_filename)
    start_logging(log_file_path)
    main_do(args_parse())
except Exception as e:
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    backup_shared.log_error_and_print(logger, "File \"{}\", line {}".format(fname, exc_tb.tb_lineno))
    backup_shared.log_error_and_print(logger, "{}: {}".format(e.__class__.__name__, str(e)))
