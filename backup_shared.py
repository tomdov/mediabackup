
# Shared lib for backup files

import os
import time
import shutil
import platform
import ctypes
import logging
import types
import functools
import json

from datetime import datetime
from subprocess import STDOUT
from subprocess import check_output
from subprocess import CalledProcessError

class Config:
    def __init__(self, config_file, env, section):
        """
        Initializes the Config object.

        Args:
            config_file (str): The path to the JSON configuration file.
            section (str): The section name within the JSON configuration file.
        """
        with open(config_file, 'r') as f:
            content     = f.read()
            self.config = json.loads(content)

        self.env     = env
        self.section = section

    def get_value(self, key):
        """
        Retrieves a value from the specified section and key.

        Args:
            key (str): The key to retrieve the value for.

        Returns:
            The value associated with the key, or None if not found.
        """
        try:
            return self.config[self.env][self.section][key]
        except KeyError:
            return None

def is_dir(dir_path):
    return os.path.isdir(dir_path)


def dir_exists(dir_path):
    return is_dir(dir_path) and os.path.exists(dir_path)


def file_exists(file_path):
    return os.path.exists(file_path)


def get_filename_from_path(file_path):
    return os.path.basename(file_path)


def get_dirname_from_path(path):
    return os.path.dirname(path)


def get_size_of_file(file_path):
    return os.path.getsize(file_path)


def get_current_date():
    return time.strftime("%Y-%m-%d")


def create_date_str_for_folder():
    return get_current_date()


def get_current_time():
    return time.strftime("%H:%M:%S")


def get_current_wd():
    return os.getcwd()


def move_file(file_path, dest_dir):
    shutil.move(file_path, dest_dir)


def remove_file(file_path):
    os.remove(file_path)


def get_lower_ext_from_file(file_path):
    _path, ext = os.path.splitext(file_path)
    return ext[1:].lower()


def days_between(d1, d2):
    d1 = datetime.strptime(d1, "%Y-%m-%d")
    d2 = datetime.strptime(d2, "%Y-%m-%d")
    return abs((d2 - d1).days)


def get_free_space_kb(dirname):
    """Return folder/drive free space (in megabytes)."""
    if platform.system() == 'Windows':
        free_bytes = ctypes.c_ulonglong(0)
        ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(dirname), None, None, ctypes.pointer(free_bytes))
        return free_bytes.value / 1024
    else:
        st = os.statvfs(dirname)
        return st.f_bavail * st.f_frsize / 1024


def get_free_space_mb(dirname):
    return get_free_space_kb(dirname) / 1024


def launch_cmd(prog_list, shell=False):
    try:
        if shell:
            ret = check_output(prog_list, shell=True, stderr=STDOUT)
        else:
            ret = check_output(prog_list, stderr=STDOUT)

    except CalledProcessError as ex:
        prog = " ".join(prog_list)
        print(f"error running process '{prog}'. return code: {str(ex.returncode)}")
        ret = ex.output
    return ret


def create_dir_if_needed(dir_path, logger = None):
    # this is a critical section when running with threads -
    # a thread might see that the path doesn't exist, then
    # another thread will create it, and the current thread
    # will try to create it and get an exception
    # therefore if you got exception - re-check if the path exists.

    try:
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
            print(dir_path + " created!")
    except:
        if logger:
            log_error(logger, "exception in create_dir_if_needed!!! ")
        if not os.path.exists(dir_path):
            raise


def init_logging(logger_name, level, log_file):
    logger = logging.getLogger(logger_name)
    logger.setLevel(level)

    fh = logging.FileHandler(log_file, encoding="UTF-8")
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s - %(thread)-6d - %(message)s')
    fh.setFormatter(formatter)

    # add the handler to logger
    logger.addHandler(fh)

    return logger


def build_log_file_path(log_dir, log_file):
    create_dir_if_needed(log_dir)
    done_dir = os.path.join(log_dir, create_date_str_for_folder())
    create_dir_if_needed(done_dir)

    log_file_path = os.path.join(done_dir, log_file)
    return log_file_path


def log_msg(logger, level, msg):
    if level == "debug":
        logger.debug(msg)
    elif level == "info":
        logger.info(msg)
    elif level == "warning":
        logger.warning(msg),
    elif level == "error":
        logger.error(msg)


def copy_func(f, name=None):
    '''
        return a function with same code, globals, defaults, closure, and
        name (or provide a new name)
        '''
    g = types.FunctionType(f.__code__, f.__globals__, name = name or f.__name__,
                            argdefs = f.__defaults__, closure = f.__closure__)
    # in case f was given attrs (note this dict is a shallow copy):
    g = functools.update_wrapper(g, f)
    g.__kwdefaults__ = f.__kwdefaults__

    return g

# create logging functions on-the-fly
log_levels = ['info', 'warning', 'error', 'debug']

for debug_level in log_levels:
    def _fn(logger, msg, level = debug_level):
        log_msg(logger, level, msg)

    # defining generic functions for logging
    globals()['log_' + debug_level] = copy_func(_fn, 'log_' + debug_level)

    # defining generic functions for logging and proting
    def _fn2(logger, msg, level = debug_level):
        log_msg(logger, level, msg)
        print(msg)

    globals()['log_' + debug_level + '_and_print'] = copy_func(_fn2, 'log_' + debug_level + '_and_print')
    del _fn
    del _fn2


def is_linux():
    return platform.system() == 'Linux'


def is_windows():
    return platform.system() == 'Windows'


def linux_default_watch_dir():
    return '/mbd/drv/wd'

def linux_default_dest_dir():
    return '/mbd/drv/dd'
