# generic interface for backup's mailer
# subclasses with concrete implementation 
# MUST call their class MailerImpl 
# in order to load it on run time

# param @data - all the "mail" json object found in config.json
#               will be passed to the mailer


class Mailer:
    def __init__(self, data):
        raise NotImplementedError

    def send_mail(self, sender, to, subject, html_content, attachments):
        raise NotImplementedError
