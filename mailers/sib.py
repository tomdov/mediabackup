# send in blue adapter
from mailers.mailer import Mailer
import sib_api_v3_sdk
import base64
import os


class MailerImpl(Mailer):
    def __init__(self, data):
        configuration = sib_api_v3_sdk.Configuration()
        configuration.api_key['api-key'] = data['mail_api_key']

        self.api_instance = sib_api_v3_sdk.TransactionalEmailsApi(sib_api_v3_sdk.ApiClient(configuration))

    def send_mail(self, sender, to, subject, html_content, attachments):
        attach_list = []
        attachments = attachments or []

        # prepare attachments
        for f in attachments:
            with open(f, 'rb') as attachment:
                base64_content = base64.b64encode(attachment.read()).decode('ascii')
                attach_list.append(sib_api_v3_sdk.SendSmtpEmailAttachment(content=base64_content, name=os.path.basename(f)))

        # send the mail
        send_smtp_email = sib_api_v3_sdk.SendSmtpEmail(to=[sib_api_v3_sdk.SendSmtpEmailTo(email=to)],
                                                       subject=subject,
                                                       sender=sib_api_v3_sdk.SendSmtpEmailSender(email=sender),
                                                       attachment=attach_list if attach_list != [] else None,
                                                       html_content=html_content)

        api_response = self.api_instance.send_transac_email(send_smtp_email)

        return api_response
