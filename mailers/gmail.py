import smtplib
import os

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
# for file attachments
from email.mime.application import MIMEApplication

#NOTE: this is currently untested (and probably broken)
# gmail has stopped supporting non-interactive apps auth


class MailerImpl:
    def __init__(self, data):
        self.mail_pass      = data["mail_pass"]
        self.mail_smtp_addr = data["mail_smtp_addr"]
        self.mail_smtp_port = data["mail_smtp_port"]
        self.mail_user      = data["mailFrom"]

    def send_mail(self, sender, to, subject, html_content, attachments):
        msgRoot = self.prepare_mail(sender, to, subject, html_content, attachments)
        smtp    = self.login()

        smtp.sendmail(sender, to, msgRoot.as_string())
        # smtp.quit()

    def login(self):
        # Send the email (this example assumes SMTP authentication is required)
        smtp = smtplib.SMTP_SSL(host=self.mail_smtp_addr, port=self.mail_smtp_port)
        smtp.connect(self.mail_smtp_addr, self.mail_smtp_port)
        smtp.login(self.mail_user, self.mail_pass)

        return smtp

    def prepare_mail(self, sender, to, subject, mail_html, attachments):
        msgRoot, msgAlternative = self.prepare_mail_send_generic(sender, to, subject)

        msgText = MIMEText(mail_html, 'html')
        msgAlternative.attach(msgText)

        add_attachments_list_to_mail(msgRoot, attachments)

        return msgRoot

    def prepare_mail_send_generic(self, fromStr, toStr, subject):
        # prepare HTML email and a plain text message for
        # email clients that don't want to display the HTML.

        # Create the root message and fill in the from, to, and subject headers
        msgRoot = MIMEMultipart('related')
        msgRoot['Subject'] = subject
        msgRoot['From'] = fromStr
        msgRoot['To'] = toStr
        msgRoot.preamble = 'This is a multi-part message in MIME format.'

        # Encapsulate the plain version of the message body in an
        # 'alternative' part, so message agents can decide which they want to display (plain/HTML).
        msgAlternative = MIMEMultipart('alternative')
        msgRoot.attach(msgAlternative)

        msgText = MIMEText('This mail doesn\'t support non-HTML readers. please have compatible HTML reader mail client.')
        msgAlternative.attach(msgText)

        return msgRoot, msgAlternative

    def add_attachments_list_to_mail(msg, attachments_list):
        for f in attachments_list:
            with open(f, "rb") as fil:
                 part = MIMEApplication(
                                        fil.read(),
                                        Name=os.path.basename(f)
                        )
            # After the file is closed
            part['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(f)
            msg.attach(part)
